Name: hsetroot
Version: 1.0.2
Release: 1%{?dist}

Summary: Wallpaper composition tool for X

License: GPLv2
URL: http://www.thegraveyard.org/hsetroot.php
Source0: ftp://ftp.netbsd.org/pub/pkgsrc/distfiles/hsetroot-1.0.2.tar.gz

BuildRequires: imlib2-devel
Requires: imlib2

%description

hsetroot is a tool which allows you to compose wallpapers ("root pixmaps") for
X. It has a lot of options like rendering gradients, solids, images but it also
allows you to perform manipulations on those things, or chain them together.
You could use one standard background image for isntance, and using tint to
make it fit your current theme. And yes, of course it is compatible with
semi-translucent applications like aterm and xchat :)

At this time, hsetroot can render: gradients (multi-color with variable
distance), solids (rectangles) and images (centered, tiled, fullscreen, or
maximum aspect). It supports the following manipulations: tinting (overlaying a
color mask), blurring, sharpening, flipping (horizontally, diagonally,
vertically) it also allows you to adjust brightness, contrast and gamma-level.
hsetroot also supports alpha-channels when rendering things.

%prep
%setup

%build
export LDFLAGS="-lX11"
%configure
make

%install
%makeinstall

%files
%defattr(-,root,root,-)
/usr/bin/hsetroot
%doc AUTHORS README ChangeLog

%changelog
* Sun Jun 21 2015 Eduardo Ito <itoed@yahoo.com> - 1.0.2
- Initial package release
