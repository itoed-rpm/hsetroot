# hsetroot RPM Package

This is an RPM package for hsetroot.

Docker and docker-dompose may be used to build the rpm:

	docker-compose run rpmbuild

## Copying

The hsetroot software is distributed under the GPLv2 license.

This packaging project is distrubuted under the ISC license.
